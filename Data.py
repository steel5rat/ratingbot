from pymongo import MongoClient
from datetime import datetime, timedelta


class Data:
    def __init__(self, connection_string):
        self.db = MongoClient(connection_string).rating_bot_modern

    def get_chat(self, chat_id):
        chat = self.db.chats.find_one({"chat_id": chat_id})
        if chat is None:
            return {"chat_id": chat_id, "processed_update_id": [], "teams": []}
        return chat

    def upsert_chat(self, chat):
        return self.db.chats.update({"chat_id": chat["chat_id"]}, chat, True)

    def try_get_tournaments(self):
        tournaments_data = self.db.tournaments_data.find_one()
        if (
            tournaments_data is None
            or tournaments_data["expiring_at"] < datetime.utcnow()
        ):
            return None
        return tournaments_data["tournaments"]

    def insert_tournaments(self, tournaments):
        self.db.tournaments_data.drop()
        self.db.tournaments_data.insert_one(
            {
                "expiring_at": datetime.utcnow() + timedelta(hours=2),
                "tournaments": tournaments,
            }
        )
