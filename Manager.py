# -*- coding: utf-8 -*-
import random


def process_getinfo(self, message, chat):
    if not message["text"].startswith("/getinfo"):
        if message["chat"]["type"] == "private":
            self.bot.send_message(
                message["chat"]["id"],
                "Возможно, вы хотели воспользоваться единственной командой, в которую я умею? Это /getinfo",
            )
        return

    teams = chat["teams"]

    if len(teams) > 0:
        buttons = generate_team_callback_data(teams)
        self.bot.send_message(
            message["chat"]["id"],
            "Найдите команду, введя первые символы ее названия, либо выберите из списка ниже.",
            buttons,
        )
    else:
        self.bot.send_message(
            message["chat"]["id"], "Найдите команду, введя первые символы ее названия."
        )

    chat["step"] = "team_id"


def process_callback_getinfo(self, callback, message, chat):
    process_callback_tournament_search(self, callback, message, chat)


def process_team_search(self, message, chat):
    search = message["text"]
    teams = self.rating.find_team(search)
    if teams is None:
        self.bot.send_message(
            message["chat"]["id"],
            "Что-то пошло не так, пробуй вводить айдишник команды еще. Если уверен в себе - пни @rubanau",
        )
        return
    if len(teams) > 20:
        self.bot.send_message(
            message["chat"]["id"],
            "Слишком много команд найдено, давайте попробуем поискать как-нибудь по-другому.",
        )
        return
    if len(teams) == 0:
        self.bot.send_message(
            message["chat"]["id"],
            "Ничего не найдено. Вы точно вводили название команды _с первой буквы_?",
        )
        return
    buttons = generate_team_callback_data(teams)
    self.bot.send_message(message["chat"]["id"], "Какие из этих ребят?", buttons)


def process_callback_team_search(self, callback, message, chat):
    data = callback["data"].split(":", 2)
    if data[0] != "t":
        return
    team_id = int(data[1])
    team_name = data[2]
    team_rating = self.rating.get_team_data(team_id)
    if team_rating is None:
        self.bot.send_message(
            message["chat"]["id"],
            "Что-то пошло не так, пробуй искать команду еще. Если уверен в себе - пни @rubanau",
        )
        return
    self.bot.send_message(
        message["chat"]["id"],
        "А теперь можно искать турнир, используя полноценный поиск по названию.",
    )
    chat["selected_team"] = {"rating": int(team_rating["rating"]), "name": team_name}
    chat["teams"] = chat["teams"][-4:]
    if (
        next((team for team in chat["teams"] if team["idteam"] == team_id), None)
        is None
    ):
        chat["teams"].append({"idteam": team_id, "name": team_name})
    chat["step"] = "tournament_id"


def process_tournament_search(self, message, chat):
    search_string = message["text"].lower()
    tournaments = self.rating.get_tournaments()
    matched_tournaments = list(
        filter(lambda t: search_string in t["name"].lower(), tournaments)
    )

    if len(matched_tournaments) > 40:
        self.bot.send_message(
            message["chat"]["id"],
            "Слишком много турниров найдено, давайте попробуем поискать как-нибудь по-другому.",
        )
        return
    if len(matched_tournaments) == 0:
        self.bot.send_message(
            message["chat"]["id"],
            "Ничего не найдено, возможно, нет инфы про сложность этого турнира. Хорошего вам дня, попробуйте поискать еще",
        )
        return
    buttons = generate_tournament_callback_data(matched_tournaments)
    self.bot.send_message(message["chat"]["id"], "Какой конкретно?", buttons)


def process_callback_tournament_search(self, callback, message, chat):
    data = callback["data"].split(":", 1)
    if data[0] != "snh":
        return
    tournament_id = int(data[1])
    tournaments = self.rating.get_tournaments()
    tournament = next((t for t in tournaments if t["_id"] == tournament_id), None)
    if tournament is not None:
        selected_team = chat["selected_team"]
        if selected_team["rating"] <= 0:
            self.bot.send_message(
                message["chat"]["id"],
                "Огого-игого")

            chat["step"] = None
            return
        number_of_uped_questions = tournament["questions_count"] - (
            tournament["difficulty_level"]
            * tournament["questions_count"]
            * 500
            / selected_team["rating"]
        )
        self.bot.send_message(
            message["chat"]["id"],
            "Чтобы сыграть в ноль '{}', команде '{}' надо брать, касатик, {:2.2f} вопросов".format(
                tournament["name"], selected_team["name"], number_of_uped_questions
            ),
        )

    else:
        self.bot.send_message(
            message["chat"]["id"],
            "Мне очень жаль, но за этот турнир ничего не скажу. Что-то пошло не так. Спасибо за использование /getinfo",
        )

    chat["step"] = None


def generate_team_callback_data(teams):
    return [
        [
            {
                "text": team["name"] + (" ({})".format(team["town"]) if team.get("town") is not None else ""),
                "callback_data": "t:" + str(team["idteam"]) + ":" + team["name"],
            }
        ]
        for team in teams
    ]


def generate_tournament_callback_data(tournaments):
    return [
        [
            {
                "text": t["name"] + " (" + t["date"] + ")",
                "callback_data": "snh:" + str(t["_id"]),
            }
        ]
        for t in tournaments
    ]


class Manager:
    def __init__(self, data, bot, rating):
        self.data = data
        self.bot = bot
        self.rating = rating
        self.message_processors = {
            None: process_getinfo,
            "team_id": process_team_search,
            "tournament_id": process_tournament_search,
        }
        self.callback_processors = {
            None: process_callback_getinfo,
            "team_id": process_callback_team_search,
            "tournament_id": process_callback_tournament_search,
        }

    def process(self, request):
        callback = request.get("callback_query")
        if callback is None:
            message = request.get("message")
        else:
            message = callback["message"]

        if message is None:
            return

        chat_from_bot = message.get("chat")
        if chat_from_bot is None or message.get("text") is None:
            return
        chat = self.data.get_chat(chat_from_bot["id"])

        message_from = message.get("from")
        if message_from is not None:
            username = message_from.get("username")
            if username is not None and username != "RatingChGKBot":
                chat["last_username"] = username

        if not self.mark_update_processed(chat, request["update_id"]):
            return

        if callback is None:
            if message["text"].startswith("/getinfo"):
                self.message_processors[None](self, message, chat)
            else:
                self.message_processors[chat.get("step")](self, message, chat)
        else:
            self.callback_processors[chat.get("step")](self, callback, message, chat)
            self.bot.answer_callback_query(callback["id"])

        self.data.upsert_chat(chat)

    @staticmethod
    def mark_update_processed(chat, update_id):
        if update_id in chat["processed_update_id"]:
            return False
        else:
            chat["processed_update_id"].append(update_id)
            chat["processed_update_id"] = chat["processed_update_id"][-50:]
            return True
