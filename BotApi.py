import requests
import json


class Api:
    api_address_format = "https://api.telegram.org/bot{}/{}"

    def __init__(self, token):
        self.token = token

    def add_webhook(self, webhookUrl):
        requests.post(
            self.api_address_format.format(self.token, "setWebhook"),
            {"url": webhookUrl},
        )

    def remove_webhook(self):
        requests.post(self.api_address_format.format(self.token, "deleteWebhook"))

    def send_message(self, chat_id, text, buttons=None):
        request = {"chat_id": chat_id, "text": text}
        if buttons is not None:
            request["reply_markup"] = json.dumps({"inline_keyboard": buttons})
        requests.post(
            self.api_address_format.format(self.token, "sendMessage"), request
        )
    
    def answer_callback_query(self, query_id):
        request = {"callback_query_id": query_id}
        requests.post(self.api_address_format.format(self.token, "answerCallbackQuery"), request)
