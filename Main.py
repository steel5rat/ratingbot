from flask import Flask, request
from BotApi import Api
from Data import Data
from Manager import Manager
from RatingApi import RatingApi
import traceback

app = Flask(__name__)

if __name__ == "__main__":
    bot = Api("111813797:AAEsLHBltlGjUYo4TGwjJ1E3tO3S2-ZBKxk")  # @sprobabot
else:
    bot = Api("888583772:AAG4AHUNjAGhWyHH_1TizVStXicgGyWZHmk")  # prod env

data = Data("mongodb+srv://ivan:BrMUlUBeGa7L2xAF@cluster0-twnr9.mongodb.net/admin")
rating = RatingApi(data)
manager = Manager(data, bot, rating)


@app.route("/healthcheck")
def healthcheck():
    return "I'm fine."


@app.route("/webhook", methods=["POST"])
def webhook():
    try:
        manager.process(request.json)
        return "Fine"
    except Exception as e:
        exception = traceback.format_exc()
        print(exception)
        return "An exception occurred", 500


if __name__ == "__main__":
    bot.add_webhook("https://dev4.rubanau.keenetic.pro/webhook")
    app.run(host="0.0.0.0", port=44444)
    bot.remove_webhook()
