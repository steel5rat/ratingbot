import requests
import json
import re
from bs4 import BeautifulSoup


class RatingApi:
    base_address = "https://rating.chgk.info/"
    team_rating_format = "api/teams.json/{}/rating"
    team_search_format = "api/teams.json/search?name={}"

    def __init__(self, data):
        self.data = data

    def get_team_data(self, team_id):
        response = requests.get(
            (self.base_address + self.team_rating_format).format(team_id)
        )
        if response.status_code != 200:
            return None
        parsed_response = json.loads(response.text)
        if len(parsed_response) == 0:
            return None
        return parsed_response[len(parsed_response) - 1]

    def find_team(self, search):
        response = requests.get(
            (self.base_address + self.team_search_format).format(search)
        )
        if response.status_code != 200:
            return None
        parsed_response = json.loads(response.text)
        return parsed_response["items"]

    def get_tournaments(self):
        cached_tournaments = self.data.try_get_tournaments()

        if cached_tournaments is not None:
            return cached_tournaments

        response_tournaments = requests.get(self.base_address + "tournaments")
        if response_tournaments.status_code != 200:
            return None
        content = response_tournaments.content.decode("utf-8")
        soup = BeautifulSoup(content, "html.parser")
        difficulties = soup.find_all(self.has_dl_class)
        results = []
        for difficult_span in difficulties:
            row = difficult_span.parent.parent
            cols = row.find_all("td")
            if len(cols) == 0 or not "{" in difficult_span.contents[0]:
                continue
            id = int(cols[0].contents[0])
            questions_numers_spans = cols[4].find_all("span")
            if len(list(questions_numers_spans)) < 2:
                continue
            name = row.find(
                lambda tag: tag.name == "a"
                and tag["href"] == "/tournament/{}".format(id)
            )
            results.append(
                {
                    "_id": id,
                    "difficulty_level": float(
                        re.split("{|}", difficult_span.contents[0])[1].replace(",", ".")
                    ),
                    "name": name.contents[0].strip(),
                    "dates_description": cols[2].find("span")["title"],
                    "date": cols[2].find("span").contents[0],
                    "questions_count": int(questions_numers_spans[1].contents[0]),
                }
            )

        self.data.insert_tournaments(results)

        return results

    @staticmethod
    def has_dl_class(tag):
        return tag.name == "span" and tag.has_attr("class") and "var-dl" in tag["class"]
