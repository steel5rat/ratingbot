import setuptools

setuptools.setup(
    name="rating-bot",
    version="0.0.1",
    author="@rubanau",
    author_email="ivan.rubanau@gmail.com",
    description="Rating bot implementation",
    long_description="Rating bot implementation",
    long_description_content_type="text/markdown",
    url="n/a",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.7",
)
